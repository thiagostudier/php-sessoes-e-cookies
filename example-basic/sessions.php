<?php

// ARMEZENAR EM ARQUIVO
// session_save_path( __DIR__ . '\sessions' );

session_start();

// CRIAR SESSÃO
// $_SESSION['meunome'] = 'Thiago';
$_SESSION['usuario'] = [
    'name' => 'Thiago',
    'age' => 21,
    'status' => true
];
// LISTAR SESSÃO
var_dump($_SESSION['usuario']);
// APAGAR SESSÃO
unset($_SESSION['usuario']);
// DESTRUIR SESSÕES
session_destroy();