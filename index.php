<?php

require __DIR__ . '/session.php';

$user = $_SESSION['user'] ?? null;

if(!$user){

    header('location: login.php');
    exit;

}

?>

<h1>Página Inicial</h1>

<p>Olá, <?=$user['email']?></p>

<a href="logout.php">Sair</a>