<?php

// CONFIGURAÇÕES DE SESSÃO

// 0 = DURARÁ ATÉ O NAVEGADOR FECHAR
// 60 = 1 MINUTO
// 60*60 = 1 HORA
// 60*60*24 = 1 DIA
// 60*60*24*7 = 1 SEMANA

// session_set_cookie_params(60*60, '/admin'); //SETAR EM DIRETÓRIO ESPECÍFICO
// session_set_cookie_params(60*60, '/', 'seusite.com'); //DOMÍNIO ESPECÍFICO
// session_set_cookie_params(60*60, '/', 'seusite.com', true); //APENAS EM HTTPS
// session_set_cookie_params(60*60, '/', 'seusite.com', false, true); //HTTP ONLY

session_set_cookie_params(0, '/', 'localhost', false, true); //HTTP ONLY

session_start();